package top.yuhaitao.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * HttpClient
 * 
 * @author ht.yu
 * @date 2017-7-30 12:53:39
 */
public class HttpUtil {
	public static final int SMALL_TIME = 5000;
	public static final int BIG_TIME = 8000;

	public static String get(String url, Map<String, String> header) throws IOException {
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		// 请求超时
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(SMALL_TIME).setConnectTimeout(SMALL_TIME).setSocketTimeout(SMALL_TIME).build();
		httpGet.setConfig(requestConfig);
		// 请求头
		if (MapUtils.isNotEmpty(header)) {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				httpGet.addHeader(entry.getKey(), entry.getValue());
			}
		}
		CloseableHttpResponse httpResponse = httpclient.execute(httpGet);
		try {
			if (HttpStatus.SC_OK == httpResponse.getStatusLine().getStatusCode()) {
				HttpEntity entity = httpResponse.getEntity();
				String string = EntityUtils.toString(entity, Consts.UTF_8);
				return string;
			}
		} finally {
			try {
				if (httpclient != null) {
					httpclient.close();
				}
				if (httpGet != null) {
					httpGet.releaseConnection();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String getJson(String url, Map<String, String> header) throws IOException {
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("accept", "json");
		// 请求超时
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(SMALL_TIME).setConnectTimeout(SMALL_TIME).setSocketTimeout(SMALL_TIME).build();
		httpGet.setConfig(requestConfig);
		// 请求头
		if (MapUtils.isNotEmpty(header)) {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				httpGet.addHeader(entry.getKey(), entry.getValue());
			}
		}
		CloseableHttpResponse httpResponse = httpclient.execute(httpGet);
		try {
			if (HttpStatus.SC_OK == httpResponse.getStatusLine().getStatusCode()) {
				HttpEntity entity = httpResponse.getEntity();
				String string = EntityUtils.toString(entity, Consts.UTF_8);
				return string;
			}
		} finally {
			try {
				if (httpclient != null) {
					httpclient.close();
				}
				if (httpGet != null) {
					httpGet.releaseConnection();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String postKeyValue(String url, Map<String, String> header, Map<String, String> param) throws IOException {
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(SMALL_TIME).setConnectTimeout(SMALL_TIME).setSocketTimeout(SMALL_TIME).build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(requestConfig);
		// 请求头
		if (MapUtils.isNotEmpty(header)) {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				httpPost.addHeader(entry.getKey(), entry.getValue());
			}
		}
		// 请求体
		if (MapUtils.isNotEmpty(param)) {
			List<NameValuePair> formparams = new ArrayList<NameValuePair>();
			for (Map.Entry<String, String> entry : param.entrySet()) {
				formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
			httpPost.setEntity(urlEncodedFormEntity);
		}
		CloseableHttpResponse httpResponse = httpclient.execute(httpPost);
		try {
			if (HttpStatus.SC_OK == httpResponse.getStatusLine().getStatusCode()) {
				HttpEntity entity = httpResponse.getEntity();
				String string = EntityUtils.toString(entity, Consts.UTF_8);
				return string;
			}
		} finally {
			try {
				if (httpclient != null) {
					httpclient.close();
				}
				if (httpPost != null) {
					httpPost.releaseConnection();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String postBody(String url, Map<String, String> header, String body) throws IOException {
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(SMALL_TIME).setConnectTimeout(SMALL_TIME).setSocketTimeout(SMALL_TIME).build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(requestConfig);
		// 请求头
		if (MapUtils.isNotEmpty(header)) {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				httpPost.addHeader(entry.getKey(), entry.getValue());
			}
		}
		// 请求体
		httpPost.setEntity(new StringEntity(body, Consts.UTF_8));
		CloseableHttpResponse httpResponse = httpclient.execute(httpPost);
		try {
			if (HttpStatus.SC_OK == httpResponse.getStatusLine().getStatusCode()) {
				HttpEntity entity = httpResponse.getEntity();
				String string = EntityUtils.toString(entity, Consts.UTF_8);
				return string;
			}
		} finally {
			try {
				if (httpclient != null) {
					httpclient.close();
				}
				if (httpPost != null) {
					httpPost.releaseConnection();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
